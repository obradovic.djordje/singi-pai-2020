import matplotlib.pyplot as plt
import numpy as np


'''
  1. ucitati podatke, izracunati i prikazati prosecne brzine po sekcijama
  2. napraviti histogram brzina ukupno i po sekcijama
  3. prikazati pomocu nekakve heet mape izgled celog skupa podataka
  4. iskoristiti kmeans da se grupisu sekcije u odnosu na histograme

'''

filename = 'C:\\Users\\djordje\\Downloads\\speeds.csv'

sections = {}
for i in range(1, 114):
    sections[i] = []
speeds = []
times = []
with open(filename) as file:
    for line in file:
        parts = line[:-1].split('\t')
        ts = float(parts[0])
        speed = float(parts[2])
        section = int(parts[3])
        sections[i].append(speed)
        if section == 76:
            speeds.append(speed)
            times.append(ts)

speeds = np.array(speeds)
hs, he = np.histogram(speeds, bins=np.arange(0, 150, 15))

print(hs)
print(he)
# plt.plot(times, speeds)
plt.bar(he[1:], hs, color='green', width=20)
plt.show()