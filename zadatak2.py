import numpy as np
import datetime
import matplotlib.pyplot as plt

stanice = [
    'C:\\data\\ATLAS\\institut_za_fiziku\\Stanica IPH\\GDAS_2017_2019_IPH.csv',
    'C:\\data\\ATLAS\\institut_za_fiziku\\Stanica LAZ\\GDAS_2018_2019_LAZ.csv',
    'C:\\data\\ATLAS\\institut_za_fiziku\\Stanica NBG\\GDAS_2017_2019_NBG.csv',
    'C:\\data\\ATLAS\\institut_za_fiziku\\Stanica OBR\\GDAS_2017_2019_OBR.csv',
    'C:\\data\\ATLAS\\institut_za_fiziku\\Stanica OVC\\GDAS_2018_2019_OVC.csv',
    'C:\\data\\ATLAS\\institut_za_fiziku\\Stanica USC\\GDAS_2018_2019_USC.csv',
    'C:\\data\\ATLAS\\institut_za_fiziku\\Stanica VEL\\GDAS_2018_2019_VEL.csv',
    'C:\\data\\ATLAS\\institut_za_fiziku\\Stanica ZEM\\GDAS_2018_2019_ZEM.csv'
]

filename = stanice[0]

data = []

def parse_date(d):
    # 1/2/2017 15:00
    d0 = datetime.datetime(2017, 1, 1, 0, 0, 0)
    parts = d.split(' ')
    date = parts[0]
    time = parts[1]

    dparts = date.split('/')
    [M,d,y] = [int(x) for x in dparts]

    tparts = time.split(':')
    [h,m] = [int(x) for x in tparts]
    d = datetime.datetime(0+y,M,d,h,m)
    delta = d - d0
    # return int(delta.total_seconds()//3600)
    return d

data = []
ddata = []
c = 0
last_day = None
day_changes = []
with open(filename) as file:
    for line in file:
        parts = line[:-1].split(',')
        if c>0:
            t = parse_date(parts[0])
            d = datetime.datetime(t.year, t.month, t.day)
            if parts[1] != '':
                el = [float(x) for x in parts[1:]]
                data.append([t, el[10]-273.15])
            # print(str(d))
            # print(str(last_day))
            # print('----------')
            if str(d) == str(last_day):
                day_changes.append(data[-1][1])
            else:
                if len(day_changes) > 0:
                    day_changes = np.array(day_changes)
                    ddata.append([d, day_changes.mean()])
                    day_changes = []
            last_day = d
            
        c += 1

data = np.array(data)
ddata = np.array(ddata)
# g = np.gradient(data[:,0])
print('ddata', ddata.shape)

plt.plot(data[:,0], data[:,1], 'r')
plt.plot(ddata[:,0], ddata[:,1], 'b')

plt.ylabel('Temperatura')
plt.show()